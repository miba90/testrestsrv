﻿using System;
using System.Collections.Generic;
using System.Web;

namespace RestService
{
    using Models.Helper;

    public static class GlobalObjects
    {
        public static HashSet<User> LoggedUsers
        {
            get
            {
                if(HttpContext.Current.Application["Users"] == null)
                    HttpContext.Current.Application["Users"] = new HashSet<User>();

                return (HashSet<User>)HttpContext.Current.Application["Users"];
            }
        }

        public static User LoggedUser
        {
            get
            {
                var session = HttpContext.Current.Session;
                if (session == null)
                    throw new Exception("Sesja nie może być nullem");
                var user = (User)session["User"];
                if(ReferenceEquals(user, null))
                    throw new Exception("Nie można pobrać pustego usera");
                return user;
            }
            set
            {
                var session = HttpContext.Current.Session;
                if (session == null)
                    throw new Exception("Sesja nie może być nullem");
                session["User"] = value;
            }
        }
    }

    public class User
    {
        public string Name { get; set; }
        public OsEnum Os { get; set; }
        public string Api { get; set; }

        protected bool Equals(User other)
        {
            return string.Equals(Name, other.Name);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
                return false;
            if (ReferenceEquals(this, obj))
                return true;
            if (obj.GetType() != this.GetType())
                return false;
            return Equals((User)obj);
        }

        public override int GetHashCode()
        {
            return (Name != null ? Name.GetHashCode() : 0);
        }

        public static bool operator ==(User u1, User u2)
        {
            return u1.Name == u2.Name;
        }

        public static bool operator !=(User u1, User u2)
        {
            return !(u1 == u2);
        }
    }
}