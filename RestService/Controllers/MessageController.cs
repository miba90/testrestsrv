﻿using System.Web.Mvc;

namespace RestService.Controllers
{
    using System.Net;
    using System.Net.Http;
    using System.Web.Http;
    using Models.Conversation;
    using Models.Helper;
    using Models.Message;
    using Newtonsoft.Json;

    public class MessageController : ApiController
    {
        [HttpPost]
        public HttpResponseMessage Index([FromBody]string value)
        {
            var input = JsonConvert.DeserializeObject<MessageInput>(value);
            var newMessage = new Message(input.ParentId, MessageTypeEnum.Msg, GlobalObjects.LoggedUser.Name, input.Msg, input.Content, 0, GlobalObjects.LoggedUser.Os, input.Location);
            MessageRepositoryHardcoded.Instance.Add(newMessage);

            return Request.CreateResponse(HttpStatusCode.OK);
        }

    }
}
