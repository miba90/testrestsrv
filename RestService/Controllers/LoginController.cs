﻿using Newtonsoft.Json;
using RestService.Models.Helper;
using RestService.Models.Login;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.SessionState;

namespace RestService.Controllers
{
    using System.Linq;

    [SessionState(SessionStateBehavior.Required)]
    public class LoginController : ApiController
    {
        // POST api/login
        public HttpResponseMessage Post([FromBody]string s)
        {
            LoginInput input = JsonConvert.DeserializeObject<LoginInput>(s);
            if(ModelState.IsValid)
            {
                if (GlobalObjects.LoggedUsers.Select(a => a.Name).Contains(input.login))
                    return this.Request.CreateResponse(System.Net.HttpStatusCode.Conflict);
                else
                {
                    var user = new User
                    {
                        Name = input.login,
                        Api = input.version,
                        Os = input.os
                    };
                    GlobalObjects.LoggedUser = user;
                    GlobalObjects.LoggedUsers.Add(user);
                }
            }

            var output = new LoginOutput200
            {
                serwerTime = Tools.ServerTime,
                version = input.version,
                isLogRcv = true,
                msg = "Elo"
            };
            var serial = JsonConvert.SerializeObject(output).Replace("\"", "'");
            return this.Request.CreateResponse(System.Net.HttpStatusCode.OK, serial);
        }
    }
}
