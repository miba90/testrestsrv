﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using RestService.Models.Update;
using Newtonsoft.Json;
using RestService.Models.Conversation;
using RestService.Models.Helper;

namespace RestService.Controllers
{
    public class UpdateController : ApiController
    {
        // POST api/update
        public HttpResponseMessage Post([FromBody]string value)
        {
            UpdateInput input = JsonConvert.DeserializeObject<UpdateInput>(value);

            IMessageRepository mr = MessageRepositoryHardcoded.Instance;
            var uo = new UpdateOutput
            {
                messages = mr.Messages,
                serverTime = Tools.ServerTime
            };

            var json = JsonConvert.SerializeObject(uo);
            json = json.Replace("\"", "'");
            return this.Request.CreateResponse(HttpStatusCode.OK, json);
        }
    }
}
