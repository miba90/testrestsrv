﻿using System.Collections.Generic;

namespace RestService.Models.Update
{
    using Conversation;

    public class UpdateOutput
    {
        public int serverTime { get; set; }
        public List<Message> messages { get; set; }
    }
}