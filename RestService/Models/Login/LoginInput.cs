﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RestService.Models.Login
{
    using Helper;

    public class LoginInput
    {
        public string version { get; set; }
        public OsEnum os { get; set; }
        public string locale { get; set; }
        public string login { get; set; }
        public string password { get; set; }
    }
}