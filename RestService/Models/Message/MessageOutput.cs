﻿namespace RestService.Models.Message
{
    using Newtonsoft.Json;

    public class MessageOutput
    {
        [JsonProperty(PropertyName = "content")]
        public int Id { get; set; }
    }
}