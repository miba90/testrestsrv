﻿namespace RestService.Models.Message
{
    using Helper;
    using Newtonsoft.Json;

    public class MessageInput
    {
        [JsonProperty(PropertyName = "parentId")]
        public int? ParentId { get; set; }

        [JsonProperty(PropertyName = "message")]
        public string Msg { get; set; }

        [JsonProperty(PropertyName = "content")]
        public byte[] Content;

        [JsonProperty(PropertyName = "location")]
        public Location Location { get; set; }
    }
}