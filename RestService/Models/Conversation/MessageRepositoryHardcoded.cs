﻿using System.Collections.Generic;
using System.Linq;
using RestService.Models.Helper;

namespace RestService.Models.Conversation
{
    public class MessageRepositoryHardcoded : IMessageRepository
    {
        private MessageRepositoryHardcoded() {}

        private static readonly MessageRepositoryHardcoded instance = new MessageRepositoryHardcoded();

        public static MessageRepositoryHardcoded Instance
        {
            get { return instance; }
        }

        private int id = 6;
        List<Message> messages = new List<Message>
        {
            new Message{MessageId = 1, Type = MessageTypeEnum.Msg, Nick = "Dżoanna", Msg = "Elo", Distance = 1000,
                Location = new Location{latitude = 0, longtitude = 0, precision = 0, source = LocationSourceEnum.Gps}, ParentId = null, SourceOs = OsEnum.Android},

            new Message{MessageId = 2, Type = MessageTypeEnum.Msg, Nick = "Jaro", Msg = "Zgubiłem psa", Distance = 100,
                Location = new Location{latitude = 0, longtitude = 0, precision = 0, source = LocationSourceEnum.Gps}, ParentId = null, SourceOs = OsEnum.Android},

            new Message{MessageId = 5, Type = MessageTypeEnum.Msg, Nick = "BigDick", Msg = "Poznajmy się", Distance = 555,
                Location = new Location{latitude = 0, longtitude = 0, precision = 0, source = LocationSourceEnum.Gps}, ParentId = 1, SourceOs = OsEnum.Web},
        };

        public List<Message> Messages
        {
            get
            {
                return messages;
            }
        }


        public void Add(Message message)
        {
            message.MessageId = id++;
            messages.Add(message);
        }


        public void Remove(int messageId)
        {
            messages.Remove(messages.Single(a => a.MessageId == messageId));
        }
    }
}