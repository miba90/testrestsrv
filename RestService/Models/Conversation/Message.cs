﻿using Newtonsoft.Json;
using RestService.Models.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RestService.Models.Conversation
{
    public class Message
    {
        [JsonProperty(PropertyName = "messageId")]
        public int MessageId { get; set; }

        [JsonProperty(PropertyName = "parentId")]
        public int? ParentId { get; set; }

        [JsonProperty(PropertyName = "type")]
        public MessageTypeEnum Type { get; set; }

        [JsonProperty(PropertyName = "nick")]
        public string Nick { get; set; }

        [JsonProperty(PropertyName = "message")]
        public string Msg { get; set; }

        [JsonProperty(PropertyName = "content")]
        public byte[] Content { get; set; }

        [JsonProperty(PropertyName = "distance")]
        public decimal Distance { get; set; }

        [JsonProperty(PropertyName = "sourceOs")]
        public OsEnum SourceOs { get; set; }

        [JsonProperty(PropertyName = "location")]
        public Location Location { get; set; }

        public Message(int? parentId, MessageTypeEnum type, string nick, string msg, byte[] content, decimal distance, OsEnum sourceOs, Location location)
        {
            ParentId = parentId;
            Type = type;
            Nick = nick;
            Msg = msg;
            Content = content;
            Distance = distance;
            SourceOs = sourceOs;
            Location = location;
        }

        public Message() {}
    }
}