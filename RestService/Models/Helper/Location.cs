﻿namespace RestService.Models.Helper
{
    public class Location
    {
        public int latitude { get; set; }
        public int longtitude { get; set; }
        public int precision { get; set; }
        public LocationSourceEnum source { get; set; }
    }
}