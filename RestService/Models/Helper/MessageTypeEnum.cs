﻿namespace RestService.Models.Helper
{
    using Newtonsoft.Json;

    public enum MessageTypeEnum
    {
        [JsonProperty(PropertyName = "MSG")]
        Msg,
        [JsonProperty(PropertyName = "NOTICE")]
        Notice
    }
}