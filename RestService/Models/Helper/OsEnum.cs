﻿namespace RestService.Models.Helper
{
    using Newtonsoft.Json;

    public enum OsEnum
    {
        [JsonProperty(PropertyName = "WEB")]
        Web,

        [JsonProperty(PropertyName = "ANDROID")]
        Android,

        [JsonProperty(PropertyName = "IOS")]
        Ios,

        [JsonProperty(PropertyName = "WM")]
        Wm
    }
}